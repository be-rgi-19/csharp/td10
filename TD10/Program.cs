﻿using System;
using TD10.ServicePav;

namespace TD10
{
    class Program
    {
        static void Main(string[] args)
        {
            ServicePav.CityNamesResponse res;
            try
            {
                PavServiceClient client = new PavServiceClient("pavsoap");

                res = client.GetCityNamesForZipCode("90001", "0");
               
                client.Close();
                if (res.ReturnCode == 0)
                {
                    Console.WriteLine("Le code postal correspond à : ");
                    foreach (City city in res.CityNames)
                    {
                        Console.WriteLine(city.Name);
                    }  
                }
                else
                {
                    Console.WriteLine("Le code postal n'est pas valide.");
                }

                Console.ReadKey();
            }
            catch (Exception e)
            {
               Console.WriteLine(e);
               Console.ReadKey();
               throw;
            }

           
        }
    }
}
